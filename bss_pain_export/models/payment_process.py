# Part of Pain Exports.
# See LICENSE file for full copyright and licensing details.

import base64
import logging
import re
import unicodedata
import xml.etree.cElementTree as ET
from datetime import datetime

import xmlschema

from odoo import _, api, fields, models
from odoo.exceptions import UserError
from odoo.modules import get_resource_path

import odoo.addons.decimal_precision as dp

from odoo.addons.bss_utils.utils.dateutils import orm2date, orm2datetime


def clean_str(elem):
    if not elem:
        return
    normal = unicodedata.normalize('NFKD', elem).encode('ASCII', 'ignore')
    return ''.join(re.findall(r"[\w\d -]", normal))


def _xml(node, *path, **attrs):
    p, pp = path[-1], path[:-1]
    return ET.SubElement(
        *(pp and (_xml(node, *pp), p, attrs) or (node, p)))


class PaymentProcess(models.Model):
    _name = 'bss_pain_export.payment_process'
    _logger = logging.getLogger(_name)
    _order = 'name desc, id desc'

    name = fields.Char(
        "Name", required=True, readonly=True,
        default=lambda self: _("New Export"))
    date_maturity = fields.Date("Date Maturity", required=True)
    date_value = fields.Date("Date Value")
    journal_id = fields.Many2one(
        'account.journal', "Bank Account", required=True,
        domain=[('type', '=', 'bank'),
                ('outbound_payment_method_ids.code', '=', 'pain')])
    date_paid = fields.Date("Date Paid")
    state = fields.Selection([
        ('draft', "Draft"),
        ('exported', "Exported"),
        ('validated', "Validated")], "State", default="draft")
    payment_line_ids = fields.One2many(
        'bss_pain_export.payment_line', 'payment_process_id', "Payment Lines")
    count_payment_lines = fields.Char(
        "Payment Lines", compute='_compute_payment_lines', readonly=True)
    amount_to_pay = fields.Float(
        "Amount To Pay", compute='_compute_payment_lines',
        digits=dp.get_precision('Account'), readonly=True)
    account_balance = fields.Float(
        "Account Balance", compute='_compute_account_balance',
        digits=dp.get_precision('Account'), readonly=True)

    @api.depends('payment_line_ids')
    def _compute_payment_lines(self):
        for p in self:
            lines = p.payment_line_ids.filtered(
                lambda r: r.selected == 'selected' and r.paid == 'paid')
            p.count_payment_lines = "{} / {}".format(
                len(lines), len(p.payment_line_ids))
            amount = 0.0
            for line in lines:
                amount += line.paid_amount
            p.amount_to_pay = amount

    def _compute_account_balance(self):
        for p in self:
            amount_field = 'balance' if (
                not p.journal_id.currency_id or
                p.journal_id.currency_id ==
                p.company_id.currency_id) else 'amount_currency'
            query = """SELECT sum(%s) FROM account_move_line
            WHERE account_id = %%s AND date <= %%s;""" % (amount_field,)
            self.env.cr.execute(
                query, (p.journal_id.default_debit_account_id.id,
                        fields.Date.today(),))
            query_results = self.env.cr.dictfetchall()
            if query_results and query_results[0].get('sum') is not None:
                p.account_balance = query_results[0].get('sum')

    def action_export(self):
        Attachment = self.env['ir.attachment']
        if self.mapped('payment_line_ids').filtered(
                lambda l: l.selected == 'selected' and not l.pain_valid):
            raise UserError(_(
                "You cannot select a line without valid bank account."))
        for p in self:
            if not p.date_value:
                raise UserError(_("Please fill in the Date Value to export."))
            doc = p._prepare_pain()
            xml_content = ET.tostring(doc, encoding='UTF-8')
            schema_path = get_resource_path(
                'bss_pain_export', 'static', 'src', 'xsd',
                'pain.001.001.03.ch.02.xsd')
            xmlschema.validate(xml_content, schema_path)
            p.name = "PAIN-{:%Y%m%d-%H%M%S}".format(
                fields.Datetime.context_timestamp(self, datetime.now()))
            Attachment.create({
                'name': _("Export File"),
                'type': 'binary',
                'datas': base64.b64encode(xml_content),
                'res_model': 'bss_pain_export.payment_process',
                'res_id': p.id,
                'mimetype': 'application/xml',
                'datas_fname': "{}.xml".format(p.name)})
            p.payment_line_ids.filtered(
                lambda r: r.selected == 'not_selected').unlink()
        self.write({'state': 'exported'})
        return True

    def action_validate(self):
        payment_method_id = self.env.ref(
            'bss_pain_export.account_payment_method_pain').id
        for payment in self:
            if not payment.date_paid:
                raise UserError(_("Please fill in the Date Paid to validate."))
            paid_lines = payment.payment_line_ids.filtered(
                lambda r: r.paid == 'paid')
            for line in paid_lines:
                pay_vals = {
                    'journal_id': self.journal_id.id,
                    'payment_method_id': payment_method_id,
                    'payment_date': self.date_paid,
                    'communication': line.communication,
                    'invoice_ids': [(4, line.invoice_id.id, None)],
                    'payment_type': 'outbound',
                    'amount': line.paid_amount,
                    'partner_id': line.partner_id.id,
                    'partner_type': 'supplier',
                }
                acc_pay = self.env['account.payment'].create(pay_vals)
                acc_pay.post()
        self.write({'state': 'validated'})
        return True

    @api.model
    def create(self, vals):
        line_ids = []
        move_lines = self.env['account.move.line'].search([
            ('account_id.internal_type', '=', 'payable'),
            ('account_id.reconcile', '=', True),
            ('journal_id.type', 'in', ['general', 'purchase']),
            ('credit', '>', 0.0),
            ('amount_residual', '!=', 0.0),
            ('move_id.state', '=', 'posted'),
            ('date_maturity', '<=', vals['date_maturity']),
            ('partner_id', '!=', False), ('invoice_id', '!=', False),
            '|',
            '&',
            ('company_currency_id.name', '=', 'CHF'),
            ('currency_id', '=', False),
            ('currency_id.name', '=', 'CHF')],
            order='date_maturity')
        treated = self.env['bss_pain_export.payment_line'].search([
            ('selected', '=', 'selected'),
            ('payment_process_state', '!=', 'validated')]
        ).mapped('move_line_id').ids
        self._logger.debug("Treated = %s", treated)
        for line in move_lines:
            if line.id in treated:
                self._logger.info("Line already treated %s", line.id)
            amount = -line.amount_residual
            line_ids.append((0, 0, {
                'date_maturity': line.date_maturity,
                'move_line_id': line.id,
                'invoice_id': line.invoice_id.id,
                'partner_id': line.partner_id.id,
                'name': "{} / {}".format(
                    line.name.replace('/', ''),
                    line.move_id.ref),
                'communication': (
                    not line.invoice_id.supplier_esr_ref) and line.move_id.ref,
                'amount_residual': amount,
                'paid_amount': amount,
                'selected': (
                    line.invoice_id.pain_partner_bank_id and 'selected' or
                    'not_selected')}))
        vals['payment_line_ids'] = line_ids
        vals['name'] = _("Draft Export")
        return super(PaymentProcess, self).create(vals)

    def unlink(self):
        for p in self:
            if p.state != 'draft':
                raise UserError(_("Cannot delete a non draft process"))
        return super(PaymentProcess, self).unlink()

    def _prepare_pain(self):
        self.ensure_one()
        IbanFormat = self.env['bss.iban_format']
        doc = ET.Element('Document')
        doc.set(
            'xmlns',
            'http://www.six-interbank-clearing.com/de/'
            'pain.001.001.03.ch.02.xsd')
        cst = _xml(doc, 'CstmrCdtTrfInitn')
        grp = _xml(cst, 'GrpHdr')
        cdate = orm2datetime(self.create_date).isoformat('T')
        _xml(grp, 'MsgId').text = "{}-{}".format(self.journal_id.name, cdate)
        _xml(grp, 'CreDtTm').text = cdate
        lines = self.payment_line_ids.filtered(
            lambda r: r.selected == 'selected')
        if not lines:
            raise UserError(_("No lines to export!"))
        _xml(grp, 'NbOfTxs').text = str(len(lines))
        amount = 0.0
        for line in lines:
            amount += line.paid_amount
        _xml(grp, 'CtrlSum').text = str(amount)
        initPty = _xml(grp, 'InitgPty')
        _xml(initPty, 'Nm').text = clean_str(self.env.user.company_id.name)
        ct = _xml(initPty, 'CtctDtls')
        _xml(ct, 'Nm').text = 'BSS Odoo PAIN Export'
        _xml(ct, 'Othr').text = self.sudo().env.ref(
            'base.module_bss_pain_export').latest_version
        pay = _xml(cst, 'PmtInf')
        _xml(pay, 'PmtInfId').text = '001'
        _xml(pay, 'PmtMtd').text = 'TRF'
        _xml(pay, 'ReqdExctnDt').text = orm2date(
            self.date_value).isoformat()
        bank_acc = self.journal_id.bank_account_id
        _xml(pay, 'Dbtr', 'Nm').text = clean_str(bank_acc.partner_id.name)
        aid = _xml(pay, 'DbtrAcct', 'Id')
        institution_id = False
        if bank_acc.acc_type == 'iban':
            iban = bank_acc.sanitized_acc_number
            institution_id = IbanFormat.get_institution_id(iban)
            _xml(aid, 'IBAN').text = iban
            # DEPRECATED: ccp remove by this module.
            # elif bank_acc.acc_type == 'ccp':
            #     institution_id = '09000'
            #     _xml(aid, 'Othr', 'Id').text = bank_acc.acc_number
        else:
            raise UserError(_("Invalid bank account."))
        fin = _xml(pay, 'DbtrAgt', 'FinInstnId')
        if bank_acc.bank_bic:
            _xml(fin, 'BIC').text = bank_acc.bank_bic
        else:
            if not institution_id:
                raise UserError(_("Missing debitor bank identification"))
            clear = _xml(fin, 'ClrSysMmbId')
            _xml(clear, 'ClrSysId', 'Cd').text = 'CHBCC'
            _xml(clear, 'MmbId').text = institution_id
        for cnt, line in enumerate(lines, 1):
            line._prepare_pain(pay, cnt)
        return doc


class PaymentLine(models.Model):
    _name = 'bss_pain_export.payment_line'
    _logger = logging.getLogger(_name)
    _order = 'date_maturity, id'

    date_maturity = fields.Date("Date Maturity", required=True)
    payment_process_id = fields.Many2one(
        'bss_pain_export.payment_process', "Payment Process",
        ondelete='cascade')
    payment_process_state = fields.Selection(
        related='payment_process_id.state')
    move_line_id = fields.Many2one(
        'account.move.line', "Move Line", readonly=True)
    invoice_id = fields.Many2one(
        'account.invoice', "Invoice", readonly=True,
        ondelete='restrict')
    bss_esr_ref = fields.Char(
        "ESR Reference", related='invoice_id.supplier_esr_ref')
    bss_esr_ref_digits = fields.Char(
        "ESR Reference", related='invoice_id.supplier_esr_ref_digits')
    esr_adherent_number = fields.Char(
        "ESR Adherent Number",
        related='invoice_id.supplier_esr_adherent_number')
    bank_account_id = fields.Many2one(
        'res.partner.bank', "Bank Account",
        related='invoice_id.pain_partner_bank_id')
    pain_valid = fields.Boolean(
        "Pain Valid", related='invoice_id.pain_valid')
    partner_id = fields.Many2one(
        'res.partner', "Partner", readonly=True, ondelete='restrict')
    selected = fields.Selection([
        ('not_selected', "Not Selected"),
        ('selected', "Selected")], "Selected", default='selected')
    paid = fields.Selection([
        ('rejected', "Rejected"),
        ('paid', "Paid")], "Paid", default='paid')
    name = fields.Char("Name", readonly=True)
    communication = fields.Char("Communication")
    amount_residual = fields.Float(
        "Amount Residual", digits=dp.get_precision('Account'), readonly=True)
    paid_amount = fields.Float(
        "Paid Amount", digits=dp.get_precision('Account'), required=True)

    @api.onchange('selected', 'paid')
    def _onchange_state(self):
        if self.payment_process_state == 'validated' and \
                (self.selected != self._origin.selected or
                 self.paid != self._origin.paid):
            return {'warning': {'title': _("Warning"), 'message': _(
                "Cannot change line state of validated payment process.")}}
        if self.selected == 'selected':
            if self._origin.move_line_id.reconciled:
                return {'warning': {'title': _("Warning"), 'message': _(
                    "Already paid")}}
            cnt = self.search([
                ('move_line_id', '=', self._origin.move_line_id.id),
                ('id', '!=', self._origin.id),
                ('selected', '=', 'selected'),
                ('payment_process_state', '!=', 'validated')], count=True)
            if cnt:
                self.selected = 'not_selected'
                return {'warning': {'title': _("Warning"), 'message': _(
                    "Line already selected in another payment process")}}

    def action_not_selected(self):
        for l in self:
            if l.payment_process_state != 'draft':
                raise UserError(
                    _("Action only available on payment process in state "
                      "'draft'"))
        return self.write({'selected': 'not_selected'})

    def action_selected(self):
        for l in self:
            if l.payment_process_state != 'draft':
                raise UserError(
                    _("Action only available on payment process in state "
                      "'draft'"))
            if l.move_line_id.reconciled:
                raise UserError(_("Already paid line %s") % l.name)
            cnt = self.search([
                ('move_line_id', '=', l.move_line_id.id),
                ('id', '!=', l.id),
                ('selected', '=', 'selected'),
                ('payment_process_state', '!=', 'validated')], count=True)
            if cnt:
                raise UserError(_(
                    "Line %s already selected in another payment process") %
                    l.name)
        return self.write({'selected': 'selected'})

    def action_rejected(self):
        for l in self:
            if l.payment_process_state != 'exported':
                raise UserError(
                    _("Action only available on payment process in state "
                      "'exported'"))
            return self.write({'paid': 'rejected'})

    def action_paid(self):
        for l in self:
            if l.payment_process_state != 'exported':
                raise UserError(
                    _("Action only available on payment process in state "
                      "'exported'"))
        return self.write({'paid': 'paid'})

    def _prepare_pain(self, pay, cnt):
        self.ensure_one()
        IbanFormat = self.env['bss.iban_format']
        cdt = _xml(pay, 'CdtTrfTxInf')
        cdt_id = _xml(cdt, 'PmtId')
        txt_id = "001-{}-{}".format(
            str(cnt).rjust(4, '0'), str(self.id).rjust(8, '0'))
        _xml(cdt_id, 'InstrId').text = txt_id
        _xml(cdt_id, 'EndToEndId').text = txt_id

        pay_code = False
        institution_id = False
        if self.bss_esr_ref_digits and self.esr_adherent_number:
            pay_code = 'CH01'
        # DEPRECATED: CH02 payment code management by this module.
        # elif self.bank_account_id.acc_type == 'ccp':
        #     pay_code = 'CH02'
        elif self.bank_account_id.acc_type == 'iban':
            institution_id = IbanFormat.get_institution_id(
                self.bank_account_id.sanitized_acc_number)
            if not institution_id:
                raise UserError(_(
                    "Missing bank identification for line %s") % self.name)
            pay_code = 'CH03'
        else:
            raise UserError(_(
                "Not implemented payment type for line %s") % self.name)
        _xml(cdt, 'PmtTpInf', 'LclInstrm', 'Prtry').text = pay_code
        _xml(cdt, 'Amt', 'InstdAmt', Ccy='CHF').text = str(self.paid_amount)
        if pay_code == 'CH03':
            fin = _xml(cdt, 'CdtrAgt', 'FinInstnId')
            clear = _xml(fin, 'ClrSysMmbId')
            _xml(clear, 'ClrSysId', 'Cd').text = 'CHBCC'
            _xml(clear, 'MmbId').text = institution_id
        cdtr = _xml(cdt, 'Cdtr')
        _xml(cdtr, 'Nm').text = clean_str(self.partner_id.name)
        if self.partner_id.street or self.partner_id.city:
            post = _xml(cdtr, 'PstlAdr')
            strt = clean_str(self.partner_id.street)
            if strt:
                _xml(post, 'StrtNm').text = strt
            if self.partner_id.zip:
                _xml(post, 'PstCd').text = self.partner_id.zip
            city = clean_str(self.partner_id.city)
            if city:
                _xml(post, 'TwnNm').text = city
            if self.partner_id.country_id:
                _xml(post, 'Ctry').text = self.partner_id.country_id.code
        aid = _xml(cdt, 'CdtrAcct', 'Id')
        # DEPRECATED: CH02 payment code management by this module.
        # if pay_code in ['CH01', 'CH02']:
        if pay_code == 'CH01':
            _xml(aid, 'Othr', 'Id').text = self.esr_adherent_number
        else:
            _xml(aid, 'IBAN').text = self.bank_account_id.sanitized_acc_number
        rmt = _xml(cdt, 'RmtInf')
        if pay_code == 'CH01':
            _xml(rmt, 'Strd', 'CdtrRefInf', 'Ref').text = (
                self.bss_esr_ref_digits)
        else:
            _xml(rmt, 'Ustrd').text = clean_str(self.communication)
