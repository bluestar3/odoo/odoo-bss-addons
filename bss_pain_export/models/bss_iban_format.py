# Part of Pain Exports.
# See LICENSE file for full copyright and licensing details.

import logging
import re

from odoo import api, fields, models


class IbanFormat(models.Model):
    _name = 'bss.iban_format'
    _logger = logging.getLogger(_name)
    _rec_name = "prefix"

    prefix = fields.Char("Prefix", required=True)
    institution_id_regex = fields.Char("Institution ID", required=True)

    def institution_id(self, iban):
        self.ensure_one()
        m = re.search(self.institution_id_regex, iban)
        if not m:
            self._logger.warning(
                "No match for pattern %s in %s",
                self.institution_id_regex, iban)
            return False
        return m.group(1)

    @api.model
    def get_institution_id(self, iban):
        self.env.cr.execute("""
            select id from bss_iban_format
            where %s like prefix || '%%';""", (iban,))
        ids = [rs[0] for rs in self.env.cr.fetchall()]
        if len(ids) == 0:
            self._logger.warning("No matching iban format for {}", iban)
            return False
        elif len(ids) > 1:
            self._logger.warning(
                "{} matching iban format for {}", len(ids), iban)
            return False
        return self.browse(ids).institution_id(iban)
