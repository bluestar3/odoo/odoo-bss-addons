# Part of Pain Exports.
# See LICENSE file for full copyright and licensing details.

import logging
import re

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools.misc import mod10r


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    _logger = logging.getLogger(_inherit)

    RE_DIGITS = re.compile(r'\d')
    RE_CCP = re.compile(r'^[0-9][0-9]-[1-9][0-9]{0,5}-[0-9]$')

    supplier_esr_ref = fields.Char("Supplier ESR Reference")
    supplier_esr_ref_digits = fields.Char(
        "Supplier ESR Reference (without spaces)", readonly=True,
        compute='_compute_supplier_esr_ref_digits')
    supplier_esr_adherent_number = fields.Char(
        "Supplier ESR Adherent Number")
    pain_partner_bank_id = fields.Many2one(
        'res.partner.bank', "Bank Account for Pain Export",
        compute='_compute_pain')
    pain_valid = fields.Boolean(
        "Valid for Pain Export", compute='_compute_pain')

    @api.constrains('supplier_esr_adherent_number')
    def _check_supplier_esr_adherent_number(self):
        if not self.supplier_esr_adherent_number:
            return
        if self.RE_CCP.match(self.supplier_esr_adherent_number):
            parts = self.supplier_esr_adherent_number.split('-')
            if mod10r("{}{:0>6}".format(*parts[:2]))[-1] != parts[2]:
                raise ValidationError(_(
                    "ESR adherent number control digit is invalid."))
        else:
            raise ValidationError(_(
                "ESR adherent number must be entered as XX-XXXXXX-X."))

    @api.constrains('supplier_esr_ref', 'supplier_esr_adherent_number')
    def _check_supplier_esr_ref(self):
        if not self.supplier_esr_ref:
            return
        esr_ref = self.supplier_esr_ref_digits
        if len(esr_ref) > 27:
            raise ValidationError(_(
                "Maximum ESR reference length is 27 digits."))
        if mod10r(esr_ref[:-1:])[-1::] != esr_ref[-1::]:
            raise ValidationError(_(
                "ESR reference control digit is invalid."))
        if not self.supplier_esr_adherent_number:
            raise ValidationError(_(
                "ESR adherent number is required for ESR payments."))

    @api.onchange('partner_id', 'supplier_esr_ref')
    def _onchange_supplier_ref(self):
        if not self.supplier_esr_ref or not self.partner_id:
            self.supplier_esr_adherent_number = False
            return
        self.supplier_esr_ref = ' '.join(
            ['{}' * 2] + ['{}' * 5] * 5).format(
                *list(self.supplier_esr_ref_digits))
        if not self.supplier_esr_adherent_number:
            previous = self.search([
                '|', ('partner_id', '=', self.partner_id.id),
                '|', ('partner_id', '=', self.partner_id.parent_id.id),
                ('partner_id', 'in', self.partner_id.child_ids.ids),
                ('supplier_esr_adherent_number', '!=', False)],
                order='date desc, id desc', limit=1)
            if previous:
                self.supplier_esr_adherent_number = (
                    previous.supplier_esr_adherent_number)

    @api.depends('supplier_esr_ref')
    def _compute_supplier_esr_ref_digits(self):
        for i in self:
            digits = ''.join(self.RE_DIGITS.findall(i.supplier_esr_ref or ''))
            i.supplier_esr_ref_digits = digits and digits.rjust(27, '0')

    def _compute_pain(self):
        for i in self:
            if i.supplier_esr_ref and i.supplier_esr_adherent_number:
                i.pain_partner_bank_id = False
                i.pain_valid = True
                continue
            args = [
                '|', ('partner_id', '=', i.partner_id.id),
                '|', ('partner_id', '=', i.partner_id.parent_id.id),
                ('partner_id', 'in', i.partner_id.child_ids.ids)]
            if i.partner_bank_id:
                args += [('id', '=', i.partner_bank_id.id)]
            bank_acc = self.env['res.partner.bank'].search(args).filtered(
                lambda a: a.acc_type == 'iban')
            if len(bank_acc) == 1:
                i.pain_partner_bank_id = bank_acc
                i.pain_valid = True
            else:
                i.pain_partner_bank_id = False
                i.pain_valid = False
