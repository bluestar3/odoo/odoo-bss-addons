# Part of Partner References.
# See LICENSE file for full copyright and licensing details.

from . import models

from odoo import api, SUPERUSER_ID


def _auto_set_reference(cr, __):
    """Set a ref for all partners with empty one.
    """
    env = api.Environment(cr, SUPERUSER_ID, {})
    env['res.partner'].with_context(active_test=True).search(
        [('ref', '=', False)]).write(
        {'ref': env['ir.sequence'].get('bss.partner.ref')})
