# Part of Marital Status.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Marital Status',
    'version': '10.0.1.2.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Marital Status
==============

Add a manageable marital status list which can be linked to an OpenERP
object like partner.

The list is initialized with this standard values :

* Married
* Divorced
* Single
* PACS (civil partnership)
* Widow, widower
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.blues2.ch',
    'depends': ['sales_team'],
    'data': [
        'security/ir.model.access.csv',
        'data/bss_marital_status_data.xml',
        'views/bss_marital_status_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
    'images': ['images/marital_status_tree.png', ],
}
