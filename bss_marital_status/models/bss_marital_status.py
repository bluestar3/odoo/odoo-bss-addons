# Part of Marital Status.
# See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class MaritalStatus(models.Model):
    _name = 'bss.marital_status'
    _description = "Marital status"

    name = fields.Char("Name", size=32, required=True, translate=True)
