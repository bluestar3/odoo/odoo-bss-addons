# Part of Inactive Financial Report.
# See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class FinancialReport(models.Model):
    _inherit = "account.financial.report"

    active = fields.Boolean('Active', default=True)
