# Part of Switzerland Postal Codes.
# See LICENSE file for full copyright and licensing details.

import base64

from odoo import models
from odoo.modules.module import get_module_resource
from odoo.netsvc import logging


class BssImportStates(models.TransientModel):
    _inherit = 'bss.import_states'
    _logger = logging.getLogger(_inherit)

    def import_default_given_ch(self):
        self.ensure_one()
        filename = get_module_resource(
            'bss_city_ch', 'static/src/data', 'states_ch_201512.csv')
        self.file = base64.b64encode(open(filename).read().encode("utf-8"))
        self.import_default()
