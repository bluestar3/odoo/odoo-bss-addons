# Part of Switzerland Postal Codes.
# See LICENSE file for full copyright and licensing details.

import base64
import csv
import io
from zipfile import ZipFile, is_zipfile

from odoo import _, models
from odoo.netsvc import logging
from odoo.osv import osv


def decode(str):
    try:
        return str.decode("utf-8")
    except UnicodeDecodeError:
        return str.decode("windows-1252")


class BssImportPostalCodes(models.TransientModel):
    _inherit = 'bss.import_postal_codes'
    _logger = logging.getLogger(_inherit)

    def import_openedata_admin_ch(self):
        self.ensure_one()
        wiz = self[0]

        State = self.env['res.country.state']
        PostalCode = self.env['bss.postal_code']

        file_input = io.BytesIO(base64.b64decode(wiz.file))
        if is_zipfile(file_input):
            with ZipFile(file_input) as zip_input:
                csv_lines = decode(zip_input.read(
                    'PLZO_CSV_LV03/PLZO_CSV_LV03.csv'
                )).replace('\xef\xbb\xbf', '').strip().split('\n')
        else:
            file_input = io.BytesIO(base64.b64decode(wiz.file))
            csv_lines = decode(file_input.read()).replace(
                '\xef\xbb\xbf', '').strip().split('\n')
        reader = csv.reader(csv_lines, delimiter=';')

        headers = next(reader)
        name_index = -1
        postal_code_index = -1
        state_index = -1
        for i in range(len(headers)):
            if headers[i] == "Ortschaftsname":
                name_index = i
            if headers[i] == "PLZ":
                postal_code_index = i
            if headers[i] == "Kantonskürzel":
                state_index = i
        if name_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'Ortschaftsname'.")
            )
        if postal_code_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'PLZ'.")
            )
        if state_index == -1:
            raise osv.except_osv(
                _("Error!"),
                _("No column 'Kantonskürzel'.")
            )

        PostalCode.search(
            [('country_id', '=', wiz.country_id.id)]).unlink()

        rows = list(reader)
        total = len(rows)
        count = 1
        for row in rows:
            self._logger.info(
                "Import '%s %s' [%d/%d]", row[postal_code_index],
                row[name_index], count, total)
            states = State.search([
                ('code', '=', row[state_index]),
                ('country_id', '=', wiz.country_id.id)])
            PostalCode.create({
                'postal_code': row[postal_code_index],
                'short_name': row[name_index],
                'long_name': row[name_index],
                'country_id': wiz.country_id.id,
                'state_id': states and states[0].id,
            })
            count += 1
        return True
