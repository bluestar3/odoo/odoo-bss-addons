# Part of Fix Account Report.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Fix Account Report',
    'version': '10.0.4.7.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Fix Account Report
==================

Fix amount format for compare column in accounr reports.
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'https://bluestar.solutions',
    'depends': ['account'],
    'data': ['report/account_report_financial_templates.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
}
