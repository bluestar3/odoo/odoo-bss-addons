# Part of Mail Whitelist.
# See LICENSE file for full copyright and licensing details.

import json
import logging
import os.path

from odoo import models, tools

logger = logging.getLogger(__name__)


def load_config():
    config_file = os.path.join(os.path.expanduser("~"), 'whitelist.json')
    try:
        with open(config_file, 'r') as f:
            return json.load(f)
    except IOError:
        logger.warn(
            "%s cannot be found. By default, everything is blocked.",
            config_file)
        return {}


class MailMail(models.Model):
    _inherit = 'mail.mail'
    CONFIG = load_config()
    whitelist_active = CONFIG.get('active', True)
    whitelist = CONFIG.get('whitelist', [])
    whitelist_domains = CONFIG.get('whitelist_domains', [])
    mail_tag = CONFIG.get('mail_tag', '')

    @staticmethod
    def whitelist_not_in(email):
        return (
            email not in MailMail.whitelist and
            email.split('@')[1] not in MailMail.whitelist_domains)

    def send(self, *args, **kwargs):
        if self.mail_tag:
            for mail in self:
                mail.write({'subject': "[{}] {}".format(
                    self.mail_tag, mail.subject)})
        if self.whitelist_active:
            process_mails = self.env['mail.mail']
            for mail in self:
                emails = []
                if mail.email_cc:
                    emails += [e for e in tools.email_split(mail.email_cc)]
                if mail.email_to:
                    emails.append(mail.email_to.strip())
                for partner in mail.recipient_ids:
                    emails.append(partner.email.strip())
                if sum([self.whitelist_not_in(e) for e in emails]):
                    logger.warn("%s blocked by whitelist", emails)
                    mail.write({
                        'state': 'exception',
                        'failure_reason': 'blocked by whitelist'})
                else:
                    process_mails |= self
        else:
            process_mails = self
        return super(MailMail, process_mails).send(*args, **kwargs)
