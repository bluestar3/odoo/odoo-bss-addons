# Part of Mail Whitelist.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Mail Whitelist',
    'version': '13.0.1.0.1',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Mail Whitelist
==============
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://bluestar.solutions',
    'depends': ['mail'],
    'data': [],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
