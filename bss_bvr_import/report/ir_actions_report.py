# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models
from odoo.netsvc import logging


class IrActionsReportXMLReportlab(models.Model):

    _inherit = 'ir.actions.report.xml'
    _logger = logging.getLogger('ir.actions.report.xml.reportlab')

    report_type = fields.Selection(selection_add=[('reportlab-pdf',
                                                   'Reportlab renderer')])

    @api.model
    def _lookup_report(self, name):
        self.env.cr.execute(
            "SELECT * FROM ir_act_report_xml WHERE report_name=%s", (name,))
        report = self.env.cr.dictfetchone()
        if report and report['report_type'] == 'reportlab-pdf':
            return report['report_name']
        else:
            return super(IrActionsReportXMLReportlab, self)._lookup_report(
                name)

    def render_report(self, res_ids, name, data):
        self._logger.debug("Render report %s with %s", name, res_ids)
        return super(IrActionsReportXMLReportlab, self).render_report(
            res_ids,
            name,
            data,
        )
