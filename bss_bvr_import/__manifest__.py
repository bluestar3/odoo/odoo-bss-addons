# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'BVR Imports',
    'version': '10.0.4.7.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description':
    """A module to improve BVR imports originally from OCA""",
    'author': 'Bluestar Solutions Sàrl',
    'website': 'http://www.bluestar.solutions',
    'depends': [
        'account',
        'bss_utils',
        'bss_one2many_action',
        'bss_queue',
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/ir_rule.xml',

        'data/account.payment.method.xml',

        'views/assets_backend_templates.xml',
        'views/res_config_view.xml',
        'views/bvr_import_view.xml',
        'views/account_journal_views.xml',
        'views/account_invoice_view.xml',
        'report/invoice_bvr.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
