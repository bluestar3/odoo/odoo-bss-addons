# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

import logging
import xml.sax
from datetime import datetime
from decimal import Decimal as D

_logger = logging.getLogger(__name__)


class Camt054Handler(xml.sax.ContentHandler):
    def __init__(self):
        self.current_path = []
        self.header = {}
        self.lines = []
        self.current_line = None
        self.current_tag = ""
        self.current_content = ""
        self.current_attr = ""
        self.parsing_lines = False
        self.is_credit = True
        self.record_id = ""
        self.treatment_date = None
        self.credit_date = None
        self.amount = D(0)
        self.currency = None
        self.sum_amount = D(0)
        self.line_count = 0

    # Call when element starts
    def startElement(self, name, attrs):
        self.current_path.append(name)
        if name == 'Amt':
            if self.currency and self.currency != attrs['Ccy']:
                raise ValueError("Multiple currencies in file not supported")
            elif not self.currency:
                self.currency = attrs['Ccy']
        elif name == 'NtryDtls':
            self.parsing_lines = True
            self.current_attr = ""
            self.current_line = {
                'treatment_date': self.treatment_date,
                'credit_date': self.credit_date}
        elif name == 'Document':
            doctype = attrs.get('xsi:schemaLocation', attrs.get('xmlns'))
            if 'camt.054' not in doctype:
                raise ValueError("Not a Camt.054 file")
        else:
            self.current_attr = ""
        self.current_content = ""

    # Call when element ends
    def endElement(self, name):
        self.current_path.pop()
        if self.parsing_lines:
            if name == 'NbOfTxs':
                self.line_count += int(self.current_content)
            elif name == 'NtryDtls':
                self.parsing_lines = False
                self.amount = D(0)
            elif name == 'TxDtls':
                self.lines.append(self.current_line)
                self.current_line = {
                    'treatment_date': self.treatment_date,
                    'credit_date': self.credit_date,
                }
                self.record_id = ""
            elif name == 'CdtDbtInd':
                if 'Chrgs' in self.current_path:
                    pass
                elif self.current_content == 'CRDT' and self.is_credit:
                    self.current_line['transaction_type'] = '002'
                elif self.current_content == 'DBIT' and not self.is_credit:
                    self.current_line['transaction_type'] = '005'
                else:
                    raise ValueError(
                        "Error in Camt.054, incoherent CdtDbtInd value in "
                        "record %s " % (self.record_id, )
                    )
            elif name == 'Amt':
                if 'Chrgs' in self.current_path:
                    if 'taxes' in self.current_line:
                        self.current_line['taxes'] += D(self.current_content)
                    else:
                        self.current_line['taxes'] = D(self.current_content)
                else:
                    self.current_line['amount'] = D(self.current_content)
                    self.current_line['currency'] = self.currency
            elif name == 'AcctSvcrRef':
                self.record_id = self.current_content
                self.current_line['deposit_reference'] = self.record_id
            elif name == 'AccptncDtTm':
                self.current_line['deposit_date'] = datetime.strptime(
                    self.current_content, '%Y-%m-%dT%H:%M:%S').isoformat()
            elif name == 'Ref' and 'CdtrRefInf' in self.current_path:
                self.current_line['reference'] = self.current_content
        else:
            if name == 'CreDtTm' and 'GrpHdr' in self.current_path:
                self.header['original_date'] = datetime.strptime(
                    self.current_content, '%Y-%m-%dT%H:%M:%S').isoformat()
            elif name == 'NtryRef':
                self.header['client_reference'] = self.current_content
            elif name == 'Amt':
                if 'Chrgs' in self.current_path:
                    if 'payment_price_value' in self.header:
                        self.header['payment_price_value'] += D(
                            self.current_content)
                    else:
                        self.header['payment_price_value'] = D(
                            self.current_content)
                else:
                    self.amount = D(self.current_content)
            elif name == 'CdtDbtInd':
                if 'Chrgs' in self.current_path:
                    # Do nothing
                    pass
                else:
                    if 'transaction_type' in self.header:
                        # Already set
                        if self.current_content == 'CRDT':
                            self.is_credit = True
                            self.sum_amount += self.amount
                        elif self.current_content == 'DBIT':
                            self.is_credit = False
                            self.sum_amount -= self.amount
                        else:
                            raise ValueError(
                                "Error in Camt.054, unknown CdtDbtInd value"
                            )
                    else:
                        if self.current_content == 'CRDT':
                            self.header['transaction_type'] = '002'
                            self.is_credit = True
                            self.sum_amount += self.amount
                        elif self.current_content == 'DBIT':
                            self.header['transaction_type'] = '005'
                            self.is_credit = False
                            self.sum_amount -= self.amount
                        else:
                            raise ValueError(
                                "Error in Camt.054, unknown CdtDbtInd value"
                            )
            elif name == 'Dt':
                if 'BookgDt' in self.current_path:
                    self.treatment_date = datetime.strptime(
                        self.current_content, '%Y-%m-%d').isoformat()
                elif 'ValDt' in self.current_path:
                    self.credit_date = datetime.strptime(
                        self.current_content, '%Y-%m-%d').isoformat()
            elif name == 'DtTm':
                if 'BookgDt' in self.current_path:
                    self.treatment_date = datetime.strptime(
                        self.current_content, '%Y-%m-%dT%H:%M:%S').isoformat()
                elif 'ValDt' in self.current_path:
                    self.credit_date = datetime.strptime(
                        self.current_content, '%Y-%m-%dT%H:%M:%S').isoformat()
            elif name == 'Ntfctn':
                self.header['amount'] = self.sum_amount
                self.header['currency'] = self.currency
                self.header['line_count'] = self.line_count

    # Call when character(s) is read
    def characters(self, content):
        self.current_content += content
