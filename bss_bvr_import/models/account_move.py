# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError


class AccountMove(models.Model):
    _inherit = 'account.move'

    bvr_import_id = fields.Many2one(
        'bss.bvr_import', "BVR Import", ondelete="cascade", readonly=True)

    def open_bss_bvr_import_move(self):
        self.ensure_one()
        action = self.env.ref('account.action_move_journal_line')
        return {
            'name': action.name,
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            'res_model': action.res_model,
            'view_mode': 'form',
            'target': 'current'}

    def _set_esr_ref(self, esr_ref):
        ml_ids = self.env['account.move.line'].search(
            [('move_id', 'in', self.ids),
             ('account_id.user_type_id.type', '=',
              'receivable')]).ids
        self.env.cr.execute("""
            update account_move_line set bss_esr_ref=%s
            where id in %s;""", (esr_ref, tuple(ml_ids or (-1,))))

    def _force_cancel(self):
        """Reopen a move with all checks bypass.

        Force reopen a move without checking account rights and
        journal settings. Used to split move lines with admin.
        If you call this method you have to be sure you never change the debit
        or credit amount of a specific account at a specific date !
        """
        if not self.env.user.id == 1:
            raise UserError(_("Only main administrator can do that."))
        if self.ids:
            self._check_lock_date()
            self._cr.execute("""
                update account_move
                set state=%s
                where id in %s;""", ('draft', tuple(self.ids),))
            self.invalidate_cache()
        return True


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    bss_esr_ref = fields.Char("ESR Reference", index=True, copy=True)

    @api.constrains('bss_esr_ref', 'partner_id')
    def check_esr_ref_partner(self):
        if not self.ids:
            return
        self.env.cr.execute(
            """
            select count(d.bss_esr_ref) from (
                select bss_esr_ref from account_move_line
                where
                    bss_esr_ref in (
                        select distinct bss_esr_ref
                        from account_move_line
                        where
                            id in %s and
                            bss_esr_ref is not null) and
                    partner_id is not null
                group by bss_esr_ref
                having count(distinct partner_id) > 1) d;""",
            (tuple(self.ids),))
        if self.env.cr.fetchone()[0]:
            raise ValidationError(_(
                "An ESR Reference cannot be shared by multiple partners!"))
