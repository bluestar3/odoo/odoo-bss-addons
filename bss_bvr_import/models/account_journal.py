# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

import re

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError
from odoo.netsvc import logging
from odoo.tools.misc import mod10r


class CompanyBankAccount(models.Model):
    _inherit = 'account.journal'
    _compile_check_bvr = re.compile(r'^[0-9][0-9]-[1-9][0-9]{0,5}-[0-9]$')
    _compile_check_num = re.compile(r'^[0-9]*$')
    _logger = logging.getLogger(_inherit)

    is_esr = fields.Boolean("Is ESR", compute='_compute_is_esr')
    bvr_customer_adherent_num = fields.Char(
        "Customer Adherent Number",
        help="This is the left part of the BVR reference used to identify "
        "the customer inside the bank. Most banks use 6 characters for this. "
        "Usually empty in case of postal accounts.")
    bvr_ccp_num = fields.Char(
        "CCP number", size=11,
        help="CCP account number in format [0-9][0-9]-[1-9][0-9]{0,5}-[0-9]. "
        "The middle part may not begin by 0.")
    bvr_bank_adherent_num = fields.Char(
        "Bank Adherent Number", size=9,
        compute="_compute_bvr_bank_adherent_num",
        help="The BVR technical value of the CCP number.")
    bvr_reference_header = fields.Char(
        "BVR Reference Header",
        help="Fixed character(s) that will be used at the beginning of the "
        "BVR reference, may be used to identify different softwares that "
        "provide BVR references for the same bank account.")
    print_bank = fields.Boolean("Print Bank on BVR")

    @api.depends('inbound_payment_method_ids.code')
    def _compute_is_esr(self):
        for j in self:
            j.is_esr = j.inbound_payment_method_ids.filtered(
                lambda m: m.code == 'esr')

    @api.depends('bvr_ccp_num')
    def _compute_bvr_bank_adherent_num(self):
        for j in self:
            if j.bvr_ccp_num:
                parts = j.bvr_ccp_num.split('-')
                parts[1] = parts[1].lstrip('0')
                j.bvr_bank_adherent_num = "{:0>2}{:0>6}{}".format(*parts)
            else:
                j.bvr_bank_adherent_num = False

    @api.constrains('bvr_ccp_num')
    def _check_bvr_ccp_num(self):
        if self.is_esr:
            if not self._compile_check_bvr.match(self.bvr_ccp_num):
                raise ValidationError(
                    _("For BVR usage, your Account Number should be of the "
                      "form XX-XXXX-X! Please check this parameter with your "
                      "bank."))
            parts = self.bvr_ccp_num.split('-')
            if mod10r("{}{:0>6}".format(parts[0], parts[1]))[-1] != parts[2]:
                raise ValidationError(
                    _("For BVR usage, your Account Number is invalid "
                      "(control digit mismatch)! "
                      "Please check this parameter with your bank."))

    @api.constrains('bvr_customer_adherent_num')
    def _check_bvr_customer_adherent_num(self):
        if (self.bvr_customer_adherent_num and
                not self._compile_check_num.match(
                    self.bvr_customer_adherent_num)):
            raise ValidationError(
                _("Customer BVR adherent number should only contain digits"))

    @api.constrains('bvr_reference_header')
    def _check_bvr_reference_header(self):
        if self.bvr_reference_header and not self._compile_check_num.match(
                self.bvr_reference_header):
            raise ValidationError(
                _("BVR reference header should only contain digits"))

    def _get_payment_ref_max_length(self):
        self.ensure_one()
        return (26 - len(self.bvr_customer_adherent_num or '') -
                len(self.bvr_reference_header or ''))

    def _get_esr_ref(self, payment_ref):
        self.ensure_one()
        if self.type != 'bank' or not self.is_esr:
            raise UserError(_(
                "You cannot generate an ESR reference "
                "for a non ESR Bank Account."))
        head_ref = self.bvr_customer_adherent_num or ''
        head_ref += self.bvr_reference_header or ''
        maxlength = 26 - len(head_ref)
        ref = payment_ref.rjust(maxlength, '0')
        if len(ref) > maxlength:
            self._logger.error(
                "get_esr_invoice_reference() returned a too long "
                "reference '%s' (%d characters for maximum %d)",
                ref, len(ref), maxlength)
            raise UserError(_(
                "%s BVR reference is too long "
                "(%d characters for maximum %d)") % (
                    ref, len(ref), maxlength))
        return mod10r(head_ref + ref)
