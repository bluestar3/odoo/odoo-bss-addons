# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

from odoo import models


class ResPartner(models.Model):
    _inherit = "res.partner"

    def _get_esr_partner_ref(self):
        self.ensure_one()
        return self.company_id._get_esr_partner_ref(self)
